package main

import (
	"github.com/shipp02/go-rov/msg"
	"github.com/shipp02/go-rov/srv"
	"time"
)

func main() {
	//sr := srv.NewSR(5000)
	//defer sr.Destroy()
	//motor := parts.NewMotor("new", "1")
	//pr := srv.NewPubRouter(5000)
	start := time.Now().Second()
	//log.Printf("%p, %v\n", pr, pr)
	//push := srv.NewPusher(5000)
	//defer push.Destroy()

	sr := srv.NewPusher(5000)
	defer sr.Destroy()

	//sr.Add(motor)
	for time.Now().Second()-start < 10 {
		sr.Send(&msg.PubSub{
			Topic:    "11",
			Messages: []string{"NullPointerException"},
		})
		//sr.Listen()
	}
	//sr.Destroy()
	//buff := make([]byte, 1024)
}
